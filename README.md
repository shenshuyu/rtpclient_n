# rtpclient_n

n路rtp流依据时间戳作rtp包合并

### 相关的类
	Recviver: ES帧接收器，用于换成相同时戳的帧
	Combiner: 组合器，用于receiver的动态管理，资源回收等。
	ChannelMgr:用于rtsp通道管理，断流重拉等。
	Channel: rtsp流通道

### 编译：
	mkdir build
	cd build
	cmake .. ; make 
