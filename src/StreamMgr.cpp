#include "StreamMgr.h"
#include "Logger.h"

using namespace chlmgr;

static time_t GetUnixTime()
{
    time_t timep;
    return time(&timep);
}

bool Channel::Start(EsDataCallback cb)
{
    //call stop
    if (_streamHandle) {
        Stop();       
    }
    _streamHandle = RtspSink_OpenStream(const_cast<char *>(_url.c_str()), OnFrame, OnState, this);
    if (!_streamHandle) {return false;}
    _esCallback = cb;
    
    // reset last pts
    _lastGotTime = GetUnixTime();
    
    return true;
}

void Channel::Stop()
{
    RtspSink_CloseStream(_streamHandle);
    _streamHandle = nullptr;
}

int Channel::OnFrame(RtspFrame *frame, void *priv_data)
{
    Channel *pSelf = (Channel *)priv_data;
    if (!pSelf) {return -1;}
    pSelf->_lastGotTime = GetUnixTime();
    if (pSelf->_esCallback) {
        uint32_t frameid_ = (frame->pts + 5999) / 6000;
        printf("------------url:%s, pts:%d\n", pSelf->_url.c_str(), frame->pts);
        pSelf->_esCallback(pSelf->_channelIdx, frame->data, frame->size, frameid_);
    }
    return 0;
}

int Channel::OnState(RtspState state, void *priv_data)
{
    Channel *pSelf = (Channel *)priv_data;
    if (!pSelf) {return -1;}

    printf("url:%s, state = %d\n", pSelf->_url.c_str(), state);
    return 0;
}

ChannelMgr::ChannelMgr()
{

}

bool ChannelMgr::InitChannel(std::vector<std::string> info, CombinedDataCallback cb, void *userdata)
{
    if (info.empty()) return false;
    int idx = 0;
    for (auto &it : info) {
        _channelMgr[it] = std::make_shared<Channel>(it, idx++);
    }
    _channelNb = _channelMgr.size();

    if (_channelNb <= 0) {return false;}

    _pCombiner = std::make_shared<Combiner>(_channelNb);
    _pCombiner->SetDataCallback(cb, userdata);    
    LOG_INFO("Create Combiner!");
    
    _mainTh = std::thread(&ChannelMgr::MainLoop, this);
    LOG_INFO("Create MainLoop done!");
    
    return true;
}

void ChannelMgr::MainLoop()
{
    while (!_shutdown) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        for (auto &it : _channelMgr) {
            if (GetUnixTime() - it.second->GetLastSlicePts() > 10) {
                //call restart
                printf("restart stream ! \n");
                it.second->Start([this](int chlIdx, const uint8_t *esData, uint32_t esLen, uint64_t pts) {
                    if (_pCombiner) {_pCombiner->InputOneFrame(chlIdx, esData, esLen, pts);}
                });
            }
        }
    }
}
