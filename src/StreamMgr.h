#ifndef _stream_mgr_h
#define _stream_mgr_h

#include <iostream>
#include <string>
#include <unordered_map>
#include <memory>
#include <thread>
#include <atomic>
#include <vector>
//
#include "Combiner.h"
#include "rtspsink.h"

namespace chlmgr {

using EsDataCallback = std::function<void(int chlIdx, const uint8_t *esData, uint32_t esLen, uint64_t pts)>;

class Channel {
public:
    Channel(std::string url, uint32_t nb) {
        _url = url;
        _channelIdx = nb;
        _bRunFlag = false;
        _lastGotTime = 0;
    }
    
    std::string GetUrl() {return _url;}
    time_t  GetLastSlicePts() {return _lastGotTime;}
    bool Start(EsDataCallback cb);
    void Stop();
private:    
    static int OnFrame(RtspFrame *frame, void *priv_data);
    static int OnState(RtspState state, void *priv_data);
private:
    uint32_t        _channelIdx;
    std::string     _url;
    RtspSink *      _streamHandle = nullptr;
    time_t          _lastGotTime;
    bool            _bRunFlag;
    EsDataCallback  _esCallback;
};

class ChannelMgr {
public:
    ChannelMgr();
    bool InitChannel(std::vector<std::string> info, CombinedDataCallback cb, void *userdata);
private:
    void MainLoop();
private:
    uint32_t                    _channelNb = 0;
    bool                        _shutdown = false;
    std::thread                 _mainTh; 
    std::shared_ptr<Combiner>   _pCombiner = nullptr;
    std::unordered_map<std::string, std::shared_ptr<Channel>> _channelMgr;
};
} //endof namespace chlmgr
#endif
