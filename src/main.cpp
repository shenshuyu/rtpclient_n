#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
//
#include "Logger.h"
#include "StreamMgr.h"
#include "Combiner.h"

using namespace chlmgr;

static void CombinedDataCb(const TargetCombinedData &data, void *userdata)
{
    printf("Combind done, channelnb:%d, pts:%d \n", data.data.size(), data.pts);
    return ;
    for (auto &t: data.data) {
        printf("``length:%d ", t.size());
    }
    printf("\n");
}

#define MAIN_OR_SUB "0"
int main()
{
    std::shared_ptr<ChannelMgr> pMgr = std::make_shared<ChannelMgr>();
    std::vector<std::string> source_;
#if 1
    source_.push_back("rtsp://10.100.7.34/1/" MAIN_OR_SUB);   
    source_.push_back("rtsp://10.100.7.34/2/" MAIN_OR_SUB);
    source_.push_back("rtsp://10.100.7.34/3/" MAIN_OR_SUB);     
    source_.push_back("rtsp://10.100.7.34/4/" MAIN_OR_SUB);
#endif
#if 1
    source_.push_back("rtsp://10.100.7.34:8554/5/" MAIN_OR_SUB);
    source_.push_back("rtsp://10.100.7.34:8554/6/" MAIN_OR_SUB);
    source_.push_back("rtsp://10.100.7.34:8554/7/" MAIN_OR_SUB);
    source_.push_back("rtsp://10.100.7.34:8554/8/" MAIN_OR_SUB);
#endif 
    if (!pMgr->InitChannel(source_, CombinedDataCb, NULL)) {
        LOG_ERROR("init channel fail!");
        return -1;
    }

    for (;;) {std::this_thread::sleep_for(std::chrono::seconds(1));}
    return 0;
}
