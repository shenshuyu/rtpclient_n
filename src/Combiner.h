#ifndef _combiner_h
#define _combiner_h
#include <iostream>
#include <string>
#include <map>
#include <list>
#include <memory>
#include <thread>
#include <atomic>
#include <vector>
#include <mutex>

typedef std::vector<char>  _es;
struct TargetCombinedData {
    uint32_t            pts;
    std::vector<_es>    data;
};

using CombinedDataCallback = std::function<void(const TargetCombinedData &data, void *userdata)>;

struct Recviver {
public:
    Recviver() = delete;
    Recviver(uint32_t pts, int32_t nb, CombinedDataCallback cb, void *user);
    void ReInit(uint32_t pts, int32_t nb, CombinedDataCallback cb, void *user, bool &normalResetFlag);
    ~Recviver();
    void InputEs(int chlIdx, const uint8_t *esData, uint32_t esLen);
    uint32_t GetPts() {   return _pts;}
    
private:
    uint32_t                _pts;
    uint32_t                _channelNb;
    CombinedDataCallback    _cb = nullptr;
    void *                  _user = nullptr;
    uint64_t                _timeoutMs;
    TargetCombinedData      _targetdata;
};

class Combiner {
public:
    Combiner(uint32_t nb);
    void SetDataCallback(CombinedDataCallback cb, void *user) {
        _cb = cb;
        _user = user;
    }
    void InputOneFrame(int chlIdx, const uint8_t *esData, uint32_t esLen, uint32_t pts);
private:
    int                     _channel_nb;
    CombinedDataCallback    _cb = nullptr;
    void *                  _user = nullptr;
    std::vector<std::shared_ptr<Recviver>>     _recviverLst;
    std::mutex              _mutex;

    uint32_t                _succCount = 0;
    uint32_t                _failCount = 0;
};
#endif
