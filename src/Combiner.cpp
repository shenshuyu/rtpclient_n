#include "Combiner.h"
#include "Logger.h"
#include <sys/time.h>

Recviver::Recviver(uint32_t pts, int32_t nb, CombinedDataCallback cb, void *user)
{
    _pts        = pts;
    _channelNb  = nb;
    _cb         = cb;
    _user       = user;
    _targetdata.pts = pts;

    _targetdata.data.resize(nb);
    printf("Create new receiver, pts:%d, channel number:%d\n", pts, nb);
}

void Recviver::ReInit(uint32_t pts, int32_t nb, CombinedDataCallback cb, void *user, bool &normalResetFlag)
{
    normalResetFlag = true;
    //error log here
    int idx = 0;
    for (auto &t : _targetdata.data) {
        if (!t.empty()) {
            normalResetFlag = false;
            LOG_ERROR("replace %d -> %d  error, Carry valid data when destroyed!, idx = %d, size:%d\n",
                _pts, pts, idx++, t.size());
        }
        t.clear();
    }
    
    _pts        = pts;
    _channelNb  = nb;
    _cb         = cb;
    _user       = user;
    _targetdata.pts = pts;

    _targetdata.data.resize(nb);
}

Recviver::~Recviver()
{
    std::cout << "release receiver ! \n" << std::endl;
}

void Recviver::InputEs(int chlIdx, const uint8_t *esData, uint32_t esLen)
{
    if (chlIdx < 0 || chlIdx >= _channelNb || esLen > 2 * 1024 * 1024 || !esData) {
        printf("fail, InputEs Param not true, chlIdx:%d\n", chlIdx);
        return ;
    }

    _targetdata.data[chlIdx].resize(esLen);
    
    memcpy(&_targetdata.data[chlIdx][0], esData, esLen);

    for (auto &t : _targetdata.data) {
        if (t.empty()) return;
    }
    if (_cb) {
        _cb(_targetdata, _user);
    }
    //clean buffer
    for (auto &t : _targetdata.data) {        
        t.clear();
    }
}

Combiner::Combiner(uint32_t nb)
{
    _channel_nb = nb;
    LOG_INFO("Create combiner, channel number is:%d", nb);
}

#define MAX_FRAME_CACHED 100
void Combiner::InputOneFrame(int chlIdx, const uint8_t *esData, uint32_t esLen, uint32_t pts)
{ 
    std::lock_guard<std::mutex> l(_mutex);    

    auto it_oldest = _recviverLst.begin();
    for (auto &it : _recviverLst) {
        if (it->GetPts() == pts) {
            it->InputEs(chlIdx, esData, esLen);
            return ;
        }
        if (it->GetPts() < (*it_oldest)->GetPts()) {*it_oldest = it;}
    }
    if (_recviverLst.size() < MAX_FRAME_CACHED) {
        //new one
        std::shared_ptr<Recviver> pt = std::make_shared<Recviver>(pts, _channel_nb, _cb, _user);
        pt->InputEs(chlIdx, esData, esLen);
        _recviverLst.push_back(pt);
    } else {
        //reuse
        bool flag;
        (*it_oldest)->ReInit(pts, _channel_nb, _cb, _user, flag);
        (*it_oldest)->InputEs(chlIdx, esData, esLen);

        if (flag) _succCount++;
        else {_failCount ++; printf("######### fail count :%d \n", _failCount);}
        printf("`````````````````````````success count %d, fail count:%d \n", _succCount, _failCount);
    }
    return ;
}
